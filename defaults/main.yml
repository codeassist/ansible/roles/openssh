---

# If 'false' - only 'inet' family will be allowed, set to 'true' if IPv6 is needed
openssh_server_ipv6_enabled: no


openssh_server_ciphers_53_default:
  - aes256-ctr
  - aes192-ctr
  - aes128-ctr
openssh_server_ciphers_53_weak: "{{ openssh_ciphers_53_default + ['aes256-cbc', 'aes192-cbc', 'aes128-cbc'] }}"

openssh_server_ciphers_66_default:
  - chacha20-poly1305@openssh.com
  - aes256-gcm@openssh.com
  - aes128-gcm@openssh.com
  - aes256-ctr
  - aes192-ctr
  - aes128-ctr
openssh_server_ciphers_66_weak: "{{ openssh_ciphers_66_default + ['aes256-cbc', 'aes192-cbc', 'aes128-cbc'] }}"

# If your clients don't support CTR (eg older versions), cbc will be added. CBC: is true if you want to connect with
# OpenSSL-base libraries e.g. ruby Net::SSH::Transport::CipherFactory requires cbc-versions of the given openssh
# ciphers to work see: http://net-ssh.github.com/net-ssh/classes/Net/SSH/Transport/CipherFactory.html
openssh_server_weak_ciphers_allowed: no


openssh_server_macs_53_default: "{{ ['hmac-sha2-512', 'hmac-sha2-256'] if (ansible_distribution|lower in ['centos', 'redhat'] and ansible_distribution_version is version('6.5', '>=')) else ['hmac-ripemd160', 'hmac-sha1'] }}"

openssh_server_macs_59_default:
  - hmac-sha2-512
  - hmac-sha2-256
  - hmac-ripemd160
openssh_server_macs_59_weak: "{{ openssh_server_macs_59_default + ['hmac-sha1'] }}"

openssh_server_macs_66_default:
  - hmac-sha2-512-etm@openssh.com
  - hmac-sha2-256-etm@openssh.com
  - umac-128-etm@openssh.com
  - hmac-sha2-512
  - hmac-sha2-256
openssh_server_macs_66_weak: "{{ openssh_server_macs_66_default + ['hmac-sha1'] }}"

openssh_server_macs_76_default:
  - hmac-sha2-512-etm@openssh.com
  - hmac-sha2-256-etm@openssh.com
  - umac-128-etm@openssh.com
  - hmac-sha2-512
  - hmac-sha2-256
# Set to "True" if weaker HMAC mechanisms are required. This is usually only necessary, if older M2M mechanism need to
# communicate with SSH, that don't have any of the configured secure HMACs enabled.
openssh_server_weak_macs_allowed: no


openssh_server_kexs_59_default:
  - diffie-hellman-group-exchange-sha256
openssh_server_kexs_59_weak: "{{ openssh_server_kexs_59_default + ['diffie-hellman-group14-sha1', 'diffie-hellman-group-exchange-sha1', 'diffie-hellman-group1-sha1'] }}"

openssh_server_kexs_66_default:
  - curve25519-sha256@libssh.org
  - diffie-hellman-group-exchange-sha256
openssh_server_kexs_66_weak: "{{ openssh_server_kexs_66_default + ['diffie-hellman-group14-sha1', 'diffie-hellman-group-exchange-sha1', 'diffie-hellman-group1-sha1'] }}"

openssh_server_kexs_80_default:
  - sntrup4591761x25519-sha512@tinyssh.org
  - curve25519-sha256@libssh.org
  - diffie-hellman-group-exchange-sha256

# Set to "true" if weaker Key-Exchange (KEX) mechanisms are required. This is usually only necessary, if older M2M
# mechanism need to communicate with SSH, that don't have any of the configured secure KEXs enabled.
openssh_server_weak_kexs_allowed: no

openssh_server_moduli_min_length: 2048

# disable CRYPTO_POLICY to take settings from sshd configuration
# see: https://access.redhat.com/solutions/4410591
openssh_server_disable_crypto_policy: yes


# Custom options for SSH daemon configuration file
openssh_server_custom_options: []


# -----------------------------
# "Basic configuration" section
# -----------------------------

# Ports to which ssh-server should listen to
openssh_server_ports_list: ['22']
# One or more ip addresses, to which ssh-server should listen to. Default is empty, but should be configured for
# security reasons!
openssh_server_listen_interfaces_list: ['0.0.0.0']
#
openssh_server_syslog_facility: AUTH
openssl_server_loglevel: INFO


# ---------------------------------
# "Security configuration" section
# ---------------------------------

# -----------------
# "Authentication"
# -----------------

# Set this to "without-password" or "yes" to allow root to login via key-based mechanism.
openssh_server_permit_root_login: ""

# An appropriate method will be used to separate priveleges ("UsePrivilegeSeparation" parameter) depending on OS
# and OpenSSH server version
openssh_server_privilege_separation: "{{ 'yes' if (ansible_distribution|lower == 'debian' and ansible_distribution_major_version is version('6','<=')) or
                                            (ansible_os_family|lower == 'redhat' and ansible_distribution_major_version is version_compare('6','<=')) else
                                         'sandbox' }}"

# Specifies the time allowed for successful authentication to the SSH server
openssh_server_login_grace_time: "60s"

# Specifies  the  maximum  number  of authentication attempts permitted per connection. Once the number of failures
# reaches half this value, additional failures are logged.
openssh_server_max_auth_retries: 3

# Maximum number of concurrent unauthenticated connections to the SSH daemon
openssh_server_max_startups: "10:30:100"

# Set this to "true" to enable PAM authentication, account processing, and session processing. If this is enabled,
# PAM authentication will be allowed through the "ChallengeResponseAuthentication" and "PasswordAuthentication".
# Depending on your PAM configuration, PAM authentication via "ChallengeResponseAuthentication" may even bypass the
# setting of "PermitRootLogin without-password". If you just want the "PAM" account and session checks to run without
# PAM authentication, then enable this but set "PasswordAuthentication" and "ChallengeResponseAuthentication" to "no".
openssh_server_use_pam_allowed: yes

# Specifies the authentication methods that must be successfully completed for a user to be granted access. This option
# must be followed by one or more lists of comma-separated authentication method names, or by the single string 'any'
# to indicate the default behaviour of accepting any single authentication method. If the default is overridden, then
# successful authentication requires completion of every method in at least one of these lists.
# For example, "publickey,password publickey,keyboard-interactive" would require the user to complete public key
# authentication, followed by either password or keyboard interactive authentication. Only methods that are next in
# one or more lists are offered at each stage, so for this example it would not be possible to attempt 'password' or
# 'keyboard-interactive' authentication before public key.
openssh_server_authentication_methods: []

openssh_server_challenge_response_auth_allowed: yes

openssh_server_keyboard_interactive_auth_allowed: yes

openssh_server_kerberos_auth_allowed: no

openssh_server_gssapi_auth_allowed: no

# If specified, login is disallowed for user names that match one of the patterns. Example:
#   openssh_server_deny_users: "foo bar"
openssh_server_deny_users: ""
# If specified, login is disallowed for users whose primary group or supplementary group list matches one of the
# patterns. Example:
#   openssh_server_deny_groups: "foo bar"
openssh_server_deny_groups: ""

# If specified, login is allowed ONLY for user names that match one of the patterns. Example:
#   openssh_server_allow_users: "root kitchen vagrant"
openssh_server_allow_users: ""
# If specified, login is allowed ONLY for users whose primary group or supplementary group list matches one of the
# patterns. Example:
#   openssh_server_allow_groups: "root kitchen vagrant"
openssh_server_allow_groups: ""

# Change default file that contains the public keys that can be used for user authentication. Example:
# openssh_server_authorized_keys_file: "/etc/ssh/authorized_keys/%u"
openssh_server_authorized_keys_file: ""

# This configuration allows you to specify a command that will run during login to retrieve a users public key file
# from a remote source and perform validation just as if the authorized_keys file was local.
# (see: https://gist.github.com/sivel/c68f601137ef9063efd7)
openssh_server_authorized_keys_command: ""
# Any non-root user that must be in the system to run the script.
openssh_server_authorized_keys_command_user: nobody


# ----------
# "Network"
# ----------

# Specifies the parameters for using "ClientAlive" messages which sends via encrypted channel
openssh_server_client_alive_interval: 60
openssh_server_client_alive_count: 3

# Allow SSH Tunnels?
openssh_server_permit_tunnel: no

# What kind of "TCP Forwarding" is allowed?
openssh_server_allow_tcp_forwarding_allowed: no

# Set to "true" to allow "Agent Forwarding".
openssh_server_agent_forwarding_allowed: no

# When "False" binding forwarded ports to non-loopback addresses is disabled. Set to true to force binding on wildcard
# address. Set to "clientspecified" to allow the client to specify which address to bind to.
openssh_server_gateway_ports: no

openssh_server_x11_forwarding_allowed: no


# -----------------------------------------
# "User environment configuration" section
# -----------------------------------------

openssh_server_permit_env_vars_from_client: no
# A string represents a list of environment variables allowed to be passed from client to the server. Example:
#   openssh_server_accept_env_vars: 'PWD HTTP_PROXY'
openssh_server_accept_env_vars: ""


# -----------------------------
# "Misc.configuration" section
# -----------------------------

# Set to "True" or value if compression is needed
openssh_server_compression_enabled: no

# Set to "True" if DNS resolutions are needed, look up the remote host name, defaults to false from 6.8, see:
#   * http://www.openssh.com/txt/release-6.8
openssh_server_use_dns_allowed: no

# Enable(True)/disable(False) printing of the MOTD
openssh_server_print_motd: yes

# Enable(True)/disable(False) display of last login information
openssh_server_print_last_log: yes

# If not empty serves a file at the specified path with ssh warning banner before authentication is allowed
openssh_server_banner_path: ""

# Enable(True)/disable(False) distribution version leakage during initial protocol handshake
openssh_server_print_debian_banner: no

## A list of public keys that are never accepted by the ssh server
openssh_server_revoked_keys_list: []


# Enable(True)/disable(False) SFTP configuration
openssh_server_sftp_enabled: yes
# Restrict SFTP connection abd access within "chroot" env only for specified user's group, e.g.:
#openssh_server_sftp_match_condition: "Group sftponly"
openssh_server_sftp_match_condition: ""
# SFTP default umask
openssh_server_sftp_umask: "0027"
# Default SFTP chroot location, e.g.:
#   openssh_server_sftp_chroot_dir: /home/%u
openssh_server_sftp_chroot_dir: ""
# List of hashes describing sub-directory inside chroot location
# openssh_server_sftp_chroot_sub_dirs_list:
#   - name: income
#     owner: user
#     group: usergroup
#     mode: '0700'
openssh_server_sftp_chroot_sub_dirs_list: []

# List of dict (containing source ip address and rules) to generate Match Address blocks for. Example:
# openssh_server_match_address_list:
#   - addr: 192.168.1.1
#     rules: ['AllowTcpForwarding yes', 'PermitRootLogin without-password']
openssh_server_match_address_list: []
# List of dict (containing group and rules) to generate Match Group blocks for. Example:
# openssh_server_match_group_list:
#   - group: 'root'
#     rules: ['AllowTcpForwarding yes']
openssh_server_match_group_list: []
# List of dict (containing user and rules) to generate Match User blocks for. Example:
# openssh_server_match_user_list:
#   - user: 'root'
#     rules: ['AllowTcpForwarding yes']
openssh_server_match_user_list: []
