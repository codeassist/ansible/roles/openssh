"""
Role tests
"""

import os
import pytest

from testinfra.utils.ansible_runner import AnsibleRunner

testinfra_hosts = AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


@pytest.mark.parametrize('file_path,distribution', [
    ('/etc/ssh/sshd_config', '*')
])
def test_configs(host, file_path, distribution):
    """
    Check if there specified content is in the config file
    """

    if distribution == '*' or distribution == host.system_info.distribution.lower():
        config_file = host.file(file_path)
        assert config_file.contains('Port 22')
        assert config_file.is_file
    else:
        pytest.skip('Test is not defined for this distribution')
