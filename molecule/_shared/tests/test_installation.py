import os
import pytest

from testinfra.utils.ansible_runner import AnsibleRunner

testinfra_hosts = AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


@pytest.mark.parametrize('pkg_name,distribution', [
    ('openssh-server', '*')
])
def test_packages(host, pkg_name, distribution):
    """
    Check if packages are installed
    """

    if distribution == '*' or distribution == host.system_info.distribution.lower():
        assert host.package(pkg_name).is_installed
    else:
        pytest.skip('Test is not defined for this distribution')
