ansible==2.9.*
ansible-lint==4.3.*
flake8==3.8.*
flake8-colors==0.1.*
molecule==3.0.*
docker==4.3.*
pytest==5.4.*
testinfra==5.2.*

