|Branch|Status|
|------|:--------:|
|master|[![pipeline status](https://gitlab.itsupportme.by/ansible-cm/roles/openssh/badges/master/pipeline.svg)](https://gitlab.itsupportme.by/ansible-cm/roles/openssh/commits/master)
|develop|[![pipeline status](https://gitlab.itsupportme.by/ansible-cm/roles/openssh/badges/develop/pipeline.svg)](https://gitlab.itsupportme.by/ansible-cm/roles/openssh/commits/develop)

Role Name
=========
### openssh

This role performs secure OpenSSH -client and -server configurations
on RHEL/CentOS and Debian/Ubuntu servers.


Requirements
------------
- Ansible 2.9 and higher


Role Variables
--------------
```yaml
# TODO
```


Dependencies
------------
None


Example Playbook
----------------
```yaml
- hosts: all
  gather_facts: yes
  tasks:
    - include_role:
        name: openssh
```


License
-------
MIT


Author Information
------------------
ITSupportMe, LLC
