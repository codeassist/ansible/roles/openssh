#!/usr/bin/env bash

virtualenv -p "$(which python3)" .venv

source .venv/bin/activate

export ANSIBLE_SKIP_CONFLICT_CHECK=1
python3 -m pip install -r molecule/requirements.txt

deactivate
